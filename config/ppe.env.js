'use strict'
module.exports = {
  NODE_ENV: '"production"',
  ROOT_API: '"https://data.europa.eu/api/hub/statistics"',
  MATOMO_URL: '"https://ppe.data.europa.eu/piwik/"',
  USERNAME_ENV: '"odportal"',
  PASSWORD_ENV: '"odp0rt4l$12"',
  SHOW_SPARQL: 'false',
}
