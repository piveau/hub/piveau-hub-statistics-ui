import Vue from 'vue'
import Router from 'vue-router'

const CurrentState = () => import(/* webpackChunkName: "CurrentState" */'@/components/CurrentState')
const Evolution = () => import(/* webpackChunkName: "Evolution" */'@/components/Evolution')

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: '/catalogue-statistics/',
  routes: [
    {
      path: '/CurrentState',
      name: 'CurrentState',
      component: CurrentState
    },
    {
      path: '/Evolution',
      name: 'Evolution',
      component: Evolution
    },
    {
      path: '*',
      redirect: { name: 'CurrentState' }
    }
  ]
})

router.beforeEach((to, from, next) => {
  document.title = to.meta.title
  next()
})

export default router
