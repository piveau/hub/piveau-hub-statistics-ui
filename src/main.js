import '@babel/polyfill'
import Vue from 'vue'
import RuntimeConfiguration from './runtimeconfig'
import App from './App'
import router from './router'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import VueI18n from 'vue-i18n'
import _ from 'lodash'
import Sticky from 'vue-sticky-directive'
import i18njson from './i18n/lang.json'
import VueMeta from 'vue-meta'
import DeuHeaderFooter from '@deu/deu-header-footer'
import UniversalPiwik from '@piveau/piveau-universal-piwik'

// Import Font Awesome Icons Library for vue
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faGoogle,
  faGooglePlus,
  faGooglePlusG,
  faFacebook,
  faFacebookF,
  faInstagram,
  faTwitter,
  faLinkedinIn
} from '@fortawesome/free-brands-svg-icons'
import {
  faComment,
  faExternalLinkAlt,
  faPlus,
  faMinus,
  faArrowDown,
  faArrowUp,
  faInfoCircle
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// Import components
const SubNav = () => import(/* webpackChunkName: "SubNav" */'./components/SubNav.vue')
const ChartContainer = () => import(/* webpackChunkName: "ChartContainer" */'./components/ChartContainer.vue')
const Countries = () => import(/* webpackChunkName: "Countries" */'./components/Countries.vue')
const CountryAndCatalogue = () => import(/* webpackChunkName: "CountryAndCatalogue" */'./components/CountryAndCatalogue.vue')
const ChartEvolutionCat = () => import(/* webpackChunkName: "ChartContainer" */'./components/ChartEvolutionCat.vue')
const ChartEvolutionCountry = () => import(/* webpackChunkName: "ChartContainer" */'./components/ChartEvolutionCountry.vue')
const ChartEvolutionCatalogues = () => import(/* webpackChunkName: "ChartContainer" */'./components/ChartEvolutionCatalogues.vue')
const ChartEvolutionTotal = () => import(/* webpackChunkName: "ChartContainer" */'./components/ChartEvolutionTotal.vue')

require('bootstrap')
require('./styles/styles.sass')
require('@deu/deu-header-footer/dist/deu-header-footer.css')

Vue.prototype.$_ = _

require('./assets/img/data-europa-logo.svg')

Vue.config.productionTip = false

library.add(faGoogle, faGooglePlus, faGooglePlusG, faFacebook, faFacebookF, faInstagram, faTwitter, faLinkedinIn, faComment, faExternalLinkAlt, faPlus, faMinus, faArrowDown, faArrowUp, faInfoCircle)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueI18n)
Vue.use(Sticky)
Vue.use(VueMeta)
Vue.use(RuntimeConfiguration, { baseConfig: process.env, debug: false })
Vue.use(DeuHeaderFooter)
Vue.use(UniversalPiwik, {
  router,
  isPiwikPro: Vue.prototype.$env.TRACKER_IS_PIWIK_PRO,
  trackerUrl: Vue.prototype.$env.TRACKER_TRACKER_URL,
  siteId: Vue.prototype.$env.TRACKER_SITE_ID,
  debug: process.env.NODE_ENV === 'development'
})

Vue.component('sub-nav', SubNav)
Vue.component('chart-container', ChartContainer)
Vue.component('countries', Countries)
Vue.component('country-catalogue', CountryAndCatalogue)
Vue.component('chart-evolution-cat', ChartEvolutionCat)
Vue.component('chart-evolution-country', ChartEvolutionCountry)
Vue.component('chart-evolution-catalogue', ChartEvolutionCatalogues)
Vue.component('chart-evolution-total', ChartEvolutionTotal)

const i18n = new VueI18n({
  locale: 'en', // set locale
  messages: i18njson // set locale messages
})

/* eslint-disable no-new */
new Vue({
  i18n,
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
